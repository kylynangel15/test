<?php

namespace Tests;

use App\Models\Category;
use App\Models\Course;
use App\Models\Module;
use App\Models\Unit;
use Drfraker\SnipeMigrations\SnipeMigrations;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication, SnipeMigrations;

    public function __construct(?string $name = null, array $data = [], string $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
    }

    public function createUnit()
    {
        $course = Course::factory()->create();
        $category = Category::factory()
            ->for($course)
            ->create();
        $module = Module::factory()
            ->for($category)
            ->create();

        return Unit::factory()
            ->for($module)
            ->create();
    }
}
