<?php

namespace Tests\Feature;

use App\Models\Role;
use App\Models\User;
use Database\Seeders\RolesTableSeeder;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class RandomFirstTenUserTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function it_can_send_ten_random_users()
    {
        $users = User::factory()->count(10)
            ->afterCreating(function ($user) {
                ray($user);
            })
            ->create();

        $user = $users->first();

        $this->assertDatabaseHas('users', [
            'name' => $user->name,
            'email' => $user->email,
            'created_at' => $user->created_at,
            'updated_at' => $user->updated_at,
        ]);

        $response = $this->actingAs($users->first())
            ->json('get', '/api/get-random-users', [
                'users' => 10,
            ]);

        $response
            ->assertStatus(200);
    }
}
