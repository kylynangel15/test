<?php

namespace Tests\Feature\Http\Controllers;

use App\Models\Dealer;
use App\Models\PhoneNumber;
use App\Models\State;
use App\Models\User;
use App\Models\VoiceMail;
use App\Services\Twilio\BuyNumberService;
use App\Services\Twilio\ReleaseNumberService;
use App\Services\Twilio\SetWebHookEndpoint;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Mockery\MockInterface;
use Tests\TestCase;

class PhoneNumberControllerTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    // we can test different parameters sent to fetch the number just to see it work or not.
    public function test_fetch_phone_numbers_and_return_paginated_records()
    {
        $user = User::factory()->create();
        PhoneNumber::factory()->count(10)->create();
        $response = $this->actingAs($user)->json('get', '/api/secret-shop-management/fetch-phone-numbers', [
            'filters'=> 'Active,Inactive',
            'total'=> '1',
            'per_page'=> '5',
            'current_page'=> '1',
            'search'=>'',
            'sortBy'=> 'created_at',
            'sortDesc'=> true,
        ]);
        $response
            ->assertStatus(200)
            ->assertJsonCount(5, 'data')
            ->assertJsonFragment([
                'total' => 10,
            ]);
    }

    public function test_search_existing_number_and_return_the_details()
    {
        $secretShopper = User::factory()->create();
        $phoneNumber = PhoneNumber::factory()->create();
        $partialOfNumber = substr($phoneNumber->value, 2, 5);
        $response = $this->actingAs($secretShopper)->json('get', '/api/secret-shop-management/phone-numbers/search', ['search' => $partialOfNumber]);
        $response
            ->assertStatus(200)
            ->assertJsonFragment([
                'value' => $phoneNumber->value,
                'formatted_value' => $phoneNumber->formatted_value,
                'id' => $phoneNumber->id,
            ]);
    }

    public function test_search_non_existing_number_and_return_the_details()
    {
        $secretShopper = User::factory()->create();
        $response = $this->actingAs($secretShopper)->json('get', '/api/secret-shop-management/phone-numbers/search', ['search' => '610']);
        $response
            ->assertStatus(200)
            ->assertJsonCount(0, 'numbers');
    }

    public function test_buy_available_number_returns_success_and_number_details()
    {
        $mockValue = new \stdClass();
        $mockValue->phoneNumber = '(500) 555-0006';
        $mockValue->sid = 1;
        $mockValue->number = '+15005550006';
        $this->mock(BuyNumberService::class, function (MockInterface $mock) use ($mockValue) {
            $mock->shouldReceive('buy')->andReturn($mockValue);
        });
        $this->mock(SetWebHookEndpoint::class)->shouldReceive('set')->andReturnNull();
        $user = User::factory()->create();
        $state = State::factory()->create();
        $phoneNumber = '+15005550006';
        $dealer = Dealer::factory()->create();
        $audio = VoiceMail::factory()->create();
        $response = $this->actingAs($user)->json('post', '/api/secret-shop-management/phone-numbers', [
            'number' => $phoneNumber,
            'state' => $state->id,
            'area_code' => $this->faker->areaCode,
            'dealer' => $dealer->id,
            'audio' => $audio->id,
            'category' => $this->faker->boolean(),
            'secret_shopper' => $user->id,
        ]);

        $response->assertStatus(200)
            ->assertJsonFragment([
                'formatted_value' => $phoneNumber,
                'value' => $audio->value,
                'name' => $dealer->name,
                'sid' => '1',
            ]);
    }

    public function test_buy_unavailable_number_aborts_the_request()
    {
        $this->mock(BuyNumberService::class, function (MockInterface $mock) {
            $mock->shouldReceive('buy')->andReturnNull();
        });
        $this->mock(SetWebHookEndpoint::class)->shouldReceive('set')->andReturnNull();
        $user = User::factory()->create();
        $state = State::factory()->create();
        $phoneNumber = '+15005550000';
        $dealer = Dealer::factory()->create();
        $audio = VoiceMail::factory()->create();
        $response = $this->actingAs($user)->json('post', '/api/secret-shop-management/phone-numbers', [
            'number' => $phoneNumber,
            'state' => $state->id,
            'area_code' => $this->faker->areaCode,
            'dealer' => $dealer->id,
            'audio' => $audio->id,
            'category' => $this->faker->boolean(),
            'secret_shopper' => $user->id,
        ]);

        $response
            ->assertStatus(500)
            ->assertJsonFragment([
                'message' => '',
            ]);
    }

    public function test_update_phone_number_and_returns_successfully()
    {
        $user = User::factory()->create();
        $existingPhoneNumber = PhoneNumber::factory()
            ->create();
        $state = State::factory()->create();
        $audio = VoiceMail::factory()->create();
        $response = $this->actingAs($user)->json('patch', "/api/secret-shop-management/phone-numbers/{$existingPhoneNumber->id}", [
            'number' => $existingPhoneNumber->value,
            'id' => $existingPhoneNumber->id,
            'state' => $state->id,
            'area_code' => $existingPhoneNumber->area_codes,
            'dealer' => $existingPhoneNumber->dealer_id,
            'audio' => $audio->id,
            'category' => $existingPhoneNumber->is_dealer,
            'secret_shopper' => $existingPhoneNumber->user_id,
        ]);

        $response->assertStatus(200)
            ->assertJsonFragment([
                'state_id' => (string) $state->id,
                'voice_mail_id' => (string) $audio->id,
                'value' => $audio->value,
                'name' => $state->name,
            ]);
    }

    public function test_delete_valid_phone_numbers_successfully()
    {
        $this->mock(ReleaseNumberService::class)->shouldReceive('release')->andReturnTrue();
        $numbers = PhoneNumber::factory()->count(3)->create();
        $ids = [
            $numbers[0]->id,
            $numbers[2]->id,
        ];
        $user = User::factory()->create();
        $response = $this->actingAs($user)->json('post', '/api/secret-shop-management/phone-numbers/delete', $ids);

        $response->assertStatus(200)
            ->assertJsonFragment([
                'success' => true,
            ]);

        $this->assertSoftDeleted($numbers[0]);
        $this->assertSoftDeleted($numbers[2]);
    }
}
