<?php

namespace Tests\Feature\Http\Controllers;

use App\Models\Category;
use App\Models\Course;
use App\Models\Module;
use App\Models\Unit;
use App\Models\User;
use App\Models\UserUnit;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class UnitFinishedWatchingControllerTest extends TestCase
{
    use RefreshDatabase;

    public function test_can_mark_the_unit_as_finished_watching_status()
    {
        $user = User::factory()->create();
        $unit = $this->createUnit();

        $response = $this->actingAs($user)
            ->post('/api/client/lms/unit/finished/watching', [
                'playlistId' => '',
                'type' => 'single',
                'unit_id' => $unit->id,
            ]);

        $userUnit = UserUnit::where('unit_id', $unit->id)
            ->first();

        $response->assertStatus(200);
        $this->assertNotNull($userUnit);
        $this->assertEquals(1, $userUnit->finished_watching);
    }
}
