<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\Course.
 *
 * @property int $id
 * @property string $name
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\CourseBuild[] $build
 * @property-read int|null $build_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Category[] $categories
 * @property-read int|null $categories_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Dealer[] $dealers
 * @property-read int|null $dealers_count
 * @method static \Illuminate\Database\Eloquent\Builder|Course newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Course newQuery()
 * @method static \Illuminate\Database\Query\Builder|Course onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|Course query()
 * @method static \Illuminate\Database\Eloquent\Builder|Course whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Course whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Course whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Course whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Course whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|Course withTrashed()
 * @method static \Illuminate\Database\Query\Builder|Course withoutTrashed()
 * @mixin \Eloquent
 */
class Course extends Model
{
    use SoftDeletes, HasFactory;

    protected $fillable = [
        'name', 'dealer_id',
    ];

    protected $with = [
        'dealers',
    ];

    public function dealers()
    {
        return $this->belongsToMany(Dealer::class, CourseDealer::class)->orderBy('dealers.name');
    }

    public function categories()
    {
        return $this->hasMany(Category::class);
    }

    public function build()
    {
        return $this->hasMany(CourseBuild::class);
    }

    public function scopeOrderByDealerName($query, $order = 'asc')
    {
        return $query
            ->leftJoin('course_dealer', 'courses.id', '=', 'course_dealer.course_id')
            ->leftJoin('dealers', 'dealers.id', '=', 'course_dealer.dealer_id')
            ->orderBy(\DB::raw('course_dealer.dealer_id IS NULL'), $order)
            ->orderBy(\DB::raw('dealers.name IS NULL, dealers.name'), $order)
            ->addSelect('courses.*')
            ->groupBy('courses.id');
    }
}
