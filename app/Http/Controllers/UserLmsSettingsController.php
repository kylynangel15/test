<?php

namespace App\Http\Controllers;

use App\Services\UserLmsSettingsService;
use Illuminate\Http\Request;

class UserLmsSettingsController extends Controller
{
    private $userLmsSettingsService;

    public function __construct(UserLmsSettingsService $userLmsSettingsService)
    {
        $this->userLmsSettingsService = $userLmsSettingsService;
    }

    public function index()
    {
        return $this->userLmsSettingsService->index();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        return $this->userLmsSettingsService->update($request);
    }
}
