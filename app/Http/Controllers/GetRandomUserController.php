<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Services\GetRandomUserService;
use Illuminate\Http\Request;

class GetRandomUserController extends Controller
{
    private $getRandomUserService;

    public function __construct(GetRandomUserService $getRandomUserService)
    {
        $this->getRandomUserService = $getRandomUserService;
    }

    public function index(Request $request)
    {
        return $this->getRandomUserService->getUsers($request->get('users') ?? 10, $request->get('search') ?? null);
    }
}
