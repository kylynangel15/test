<?php

namespace App\Http\Controllers;

use App\Models\Role;
use App\Models\User;
use App\Models\UserGoal;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class UserGoalController extends Controller
{
    public function index(Request $request)
    {
        $currentUser = auth()->user();
        $usersIds = User::select('id')->where(function ($query) use ($currentUser) {
            if ($currentUser->roles->pluck('name')->contains(Role::SPECIFIC_DEALER_MANAGER)) {
                $query->whereNotNull('users.specific_dealer_id')->where('users.specific_dealer_id', $currentUser->specific_dealer_id);
            }

            if ($currentUser->roles->pluck('name')->contains(Role::ACCOUNT_MANAGER)) {
                $query->whereNotNull('users.dealer_id')->where('users.dealer_id', $currentUser->dealer_id);
            }
        })->get()->pluck('id');
        $perPage = isset($request->per_page) ? $request->per_page : 5;
        $page = isset($request->current_page) ? $request->current_page : 1;
        $sortBy = isset($request->sortBy) ? $request->sortBy : '';
        $sortDesc = $request->has('sortDesc') && $request->sortDesc == 'true' ? 'desc' : 'asc';
        if (empty($sortBy)) {
            $sortBy = 'is_sent';
        }
        if (empty($sortDesc)) {
            $sortDesc = 'asc';
        }

        if ($sortBy == 'user.name') {
//            $goals = UserGoal::with(['user' => function ($query) use ($sortDesc) {
//                $query->orderBy('name', $sortDesc);
//            }]);
            $goals = UserGoal::with('user')->whereIn('user_id', $usersIds)->select('user_goals.*')->addSelect('users.name AS username')->join('users', 'users.id', '=', 'user_goals.user_id')->orderBy('username', $sortDesc);
        } else {
            $goals = UserGoal::with('user')->whereIn('user_id', $usersIds)->orderBy($sortBy, $sortDesc);
        }

        return response([
            'total' => $goals->count(),
            'data' => $goals->forPage($page, $perPage)->get(),
            'current_page' => $page,
            'per_page' => $perPage,
        ]);
    }

    public function store(Request $request)
    {
        try {
            $currentUserId = auth()->id();
            $record = new UserGoal;
            $record->user_id = $currentUserId;
            $record->goal = $request->goal;
            $record->save();

            return response()->json([
                'status' => 'success',
                'new_goal' => $record,
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'status' => 'failed',
            ]);
        }
    }

    public function show($id)
    {
        $user = User::find($id);
        $records = $user->goals;

        return response()->json([
            'data' => $records,
        ]);
    }

    public function assign()
    {
    }

    public function assignToAll(Request $request)
    {
        $currentUser = auth()->user();
        $users = User::withoutGlobalScopes()->select('id')->where(function ($query) use ($currentUser) {
            if ($currentUser->roles->pluck('name')->contains(Role::SPECIFIC_DEALER_MANAGER)) {
                $query->whereNotNull('users.specific_dealer_id')->where('users.specific_dealer_id', $currentUser->specific_dealer_id);
            }

            if ($currentUser->roles->pluck('name')->contains(Role::ACCOUNT_MANAGER)) {
                $query->whereNotNull('users.dealer_id')->where('users.dealer_id', $currentUser->dealer_id);
            }
        });
        $ids = $users->get()->pluck('id');
        foreach ($ids as $id) {
            $record = new UserGoal;
            $record->user_id = $id;
            $record->goal = $request->new_goal;
            $record->save();
        }

        return response()->json([
            'status' => 'success',
        ]);
    }

    public function assignToMultipleUsers(Request $request)
    {
        $ids = $request->ids;

        foreach ($ids as $id) {
            $record = new UserGoal;
            $record->user_id = $id;
            $record->goal = $request->new_goal;
            $record->save();
        }

        return response()->json([
            'status' => 'success',
        ]);
    }
}
