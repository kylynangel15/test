<?php

namespace App\Jobs;

use App\Mail\SendUserLmsReminder;
use App\Models\UserGoal;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Twilio\Rest\Client;

class SendLmsReminders implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Log::info('sending the lms reminder every minutes start.');
        $users = Cache::get('users-daily-notifications');
        if ($users == null) {
            Log::info('no users found for sending lms reminders.');

            return;
        }
        $now = now();
        $currentDay = $now->format('l');
        $currentTime = explode(':', $now->format('G:i'));
        $currentTimeInMinutes = ($currentTime[0] * 60) + $currentTime[1];
        foreach ($users as $user) {
            $settings = [
                'days' => getUserOption($user->options, 'lms_settings_days') != null ? json_decode(getUserOption($user->options, 'lms_settings_days')) : ['monday'],
                'time' => getUserOption($user->options, 'lms_settings_time') !== null ? getUserOption($user->options, 'lms_settings_time') : '09:00',
                'is_send_votd_notification' => getUserOption($user->options, 'lms_settings_votd_notification') !== null ? (bool) getUserOption($user->options, 'lms_settings_votd_notification') : true,
                'is_send_assigned_modules_notification' => getUserOption($user->options, 'lms_settings_assigned_modules_notification') !== null ? (bool) getUserOption($user->options, 'lms_settings_assigned_modules_notification') : true,
                'timezone' => getUserOption($user->options, 'lms_settings_timezone') !== null ? getUserOption($user->options, 'lms_settings_timezone') : 'America/Los_Angeles',
            ];

            $optionTime = explode(':', $settings['time']);
            $newTime = Carbon::createFromTime($optionTime[0], $optionTime[1], 0, $settings['timezone'])->toDate();
            $convertedTime = explode(':', $newTime->setTimezone(new \DateTimeZone('America/New_York'))->format('G:i'));
            $timeInMinutes = ($convertedTime[0] * 60) + $convertedTime[1];

            if (! in_array(strtolower($currentDay), $settings['days'])) {
                continue;
            }

            if ($currentTimeInMinutes != $timeInMinutes) {
                continue;
            }

            $isEmailNotificationOn = $user->mail_subscription;
            $isSmsNotificationOn = $user->sms_notification;
            $phoneNumber = $user->phone_number;
            $is_send_votd_notification = $settings['is_send_votd_notification'];
            $is_send_assigned_modules_notification = $settings['is_send_assigned_modules_notification'];
            $goals = $user->goals->where('is_sent', false)->implode('goal', '<br>');
            $content = view('mailers.lms-reminder', compact('user', 'goals', 'is_send_votd_notification', 'is_send_assigned_modules_notification'))->render();

            if (empty($goals)) {
                continue;
            }

            if ($isEmailNotificationOn) {
                Mail::to($user->email)->send(new SendUserLmsReminder($content, 'admin@webinarinc.com', 'LMS Reminder'));
            }

            if ($isSmsNotificationOn) {
                $twilio = new Client(config('twilio.sid'), config('twilio.token'));
                $goals = str_replace('<br>', "\n", $goals);
                if ($is_send_votd_notification) {
                    $goals .= "\n".'View Video Of The Day: https://app.webinarinc.com/client/lms#video-of-the-day'."\n";
                }

                $goals .= "\n".'View Assigned Units: https://app.webinarinc.com/client/lms/assigned-units'."\n";

                if ($is_send_assigned_modules_notification) {
                    $goals .= "\n".'View Assigned Modules: https://app.webinarinc.com/login'."\n";
                }

                $goals .= "\n".'Assigned Playlists: https://app.webinarinc.com/client/lms/playlists'."\n";

                $phoneNumber = preg_replace('/[^0-9.]+/', '', $phoneNumber);
                $isValidNumber = (10 == strlen($phoneNumber)) ? true : false;
                $phoneNumber = '+1'.$phoneNumber;

                if ($isValidNumber) {
                    $message = $twilio->messages
                        ->create(
                            $phoneNumber,
                            [
                                'body' => $goals,
                                'from' => '+16266281558',
                            ]
                    );
                } else {
                    Log::error('Invalid phone number.');
                }
            }
            UserGoal::whereIn('id', $user->goals->pluck('id')->all())->update(['is_sent' => true]);
        }
    }
}
