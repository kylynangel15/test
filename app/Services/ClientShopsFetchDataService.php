<?php

namespace App\Services;

use App\Exports\ReportExport\SecretShopsReport;
use App\Helpers\ClientShopsHelper;
use App\Models\InternetShop;
use Carbon\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Storage;

class ClientShopsFetchDataService
{
    private $user;
    private $clientShopsHelper;

    public function __construct(ClientShopsHelper $clientShopsHelper)
    {
        $this->user = auth()->user();
        $this->clientShopsHelper = $clientShopsHelper;
    }

    public function index($request)
    {
        $is = InternetShop::with('dealer', 'specificDealer', 'city', 'state', 'postedBy', 'source');
        $is = $this->user->specific_dealer_id ? $is->where('specific_dealer_id', $this->user->specific_dealer_id) : $is->where('dealer_id', $this->user->dealer_id);

        $internetShop = $this->clientShopsHelper->builderQuery($is, $request->all());
        $internetShop = $internetShop->get();

        return [
            'internetshops'    =>  $internetShop,
            'datasummary'  =>  [
                [
                    'title' => 'Call',
                    'icon' => 'fal fa-phone-rotary',
                    'data'  =>  $this->getData($internetShop, 'call', $request->noFilter),
                ],
                [
                    'title' => 'Email',
                    'icon' => 'fal fa-envelope',
                    'data'  =>  $this->getData($internetShop, 'email', $request->noFilter),
                ],
                [
                    'title' => 'Chat',
                    'icon' => 'fal fa-comment-dots',
                    'data'  =>  $this->getData($internetShop, 'chat', $request->noFilter),
                ],
                [
                    'title' => 'SMS',
                    'icon' => 'fal fa-sms',
                    'data'  =>  $this->getData($internetShop, 'sms', $request->noFilter),
                ],
            ],
        ];
    }

    public function exportData($request)
    {
        if (! $request->has('internetshops')) {
            return false;
        }

        $export = new SecretShopsReport($request->internetshops);
        $filename = Carbon::now()->format('Ymdhms').'-'.$this->user->id.'.xlsx';
        if (($export)->store('export/reports/'.$filename, 's3', null, ['visibility' =>  'public'])) {
            return response()->json([
                'success'   =>  true,
                'filepath'  =>  Storage::disk('s3')->url('export/reports/'.$filename),
            ]);
        }
    }

    private function tenPercent($count): int
    {
        $count = $count * .10;

        return ceil($count);
    }

    private function isValidResponseTime($time)
    {
        if (! $time) {
            return true;
        }
        $checkTime = $this->parseTimeToDate($time);
        $referenceDate = now()->startOfDay();

        $totalHours = $referenceDate->diffInHours($checkTime);
        $totalMinutes = $referenceDate->diffInMinutes($checkTime);
        $totalSeconds = $referenceDate->diffInSeconds($checkTime);

        return empty($totalHours) && empty($totalMinutes) && empty($totalSeconds);
    }

    private function averageTime(Collection $value): string
    {
        if ($value->isEmpty()) {
            return '--:--:--';
        }

        $referenceDate = now()->startOfDay();
        $totalHours = 0;
        $totalMinutes = 0;
        $totalSeconds = 0;
        $count = $value->count();

        foreach ($value as $item) {
            $totalHours += $referenceDate->diffInHours($item);
            $totalMinutes += $referenceDate->diffInMinutes($item);
            $totalSeconds += $referenceDate->diffInSeconds($item);
        }

        if (empty($totalHours) && empty($totalMinutes) && empty($totalSeconds)) {
            return '00:00:00';
        }

        $totalHours = $this->getAverage($totalHours, $count);
        $totalMinutes = $this->getAverage($totalMinutes, $count);
        $totalSeconds = $this->getAverage($totalSeconds, $count);

        return $referenceDate
            ->addHours($totalHours)
            ->addMinutes($totalMinutes)
            ->addSeconds($totalSeconds)
            ->format('H:i:s');
    }

    private function getData($internetShop, $column, $noFilter)
    {
        $columnAttempts = $column.'_attempts';
        $columnResponseTime = $column.'_response_time';

        $tenPercentC = ! $noFilter ? $this->tenPercent($internetShop->count()) : $internetShop->count();

        $attemptResults = $internetShop->pluck($columnAttempts);

        $attemptResultsTop = $attemptResults->sort()->reverse();
        $attemptResultsBtm = $attemptResults->sort();
        $attemptResultsTop = $attemptResultsTop->reject(function ($attempts) {
            return $attempts === 0;
        })->take($tenPercentC)->sum();

        $attemptResultsBtm = $attemptResultsBtm->reject(function ($attempts) {
            return $attempts === 0;
        })->take($tenPercentC)->sum();

        $responseTimeResults = $internetShop->pluck($columnResponseTime);

        $tenPercent = ! $noFilter ? $this->tenPercent($responseTimeResults->count()) : $responseTimeResults->count();
//        $responseTimeResults = $this->processResponseTime($responseTimeResults);
        $responseTimeResults = $responseTimeResults->filter();
//        $responseTimeResultsTop = $responseTimeResults->sortBy(function ($time) {
//            return $time->getTimestamp();
//        });
//        $responseTimeResultsBtm = $responseTimeResults->sortBy(function ($time) {
//            return $time->getTimestamp();
//        })->reverse();

        $responseTimeResultsTop = $responseTimeResults->sort();
        $responseTimeResultsBtm = $responseTimeResults->sortDesc();

        $responseTimeResultsTop = $responseTimeResultsTop->take($tenPercent);
        $responseTimeResultsBtm = $responseTimeResultsBtm->take($tenPercent);

        $tenPercent = ! $noFilter ? $tenPercentC : $internetShop->count();
        $attemptResultsTop = $attemptResultsTop !== 0 ? intval(ceil($attemptResultsTop / $tenPercent)) : 0;
        $attemptResultsBtm = $attemptResultsBtm !== 0 ? intval(ceil($attemptResultsBtm / $tenPercent)) : 0;

//        $avgTop = $this->averageTime($responseTimeResultsTop);
//        $avgBtm = $this->averageTime($responseTimeResultsBtm);

        $avgTop = $this->calculateAvg($responseTimeResultsTop);
        $avgBtm = $this->calculateAvg($responseTimeResultsBtm);

        $response = [
            'top_attempts'  =>  $attemptResultsTop,
            'bottom_attempts'   =>  $attemptResultsBtm,
            'top_response_time' =>  $avgTop,
            'bottom_response_time'  =>  $avgBtm,
            'tpc'   =>  $tenPercentC,
            'tp'    =>  $tenPercent,
        ];

        if ($columnResponseTime == 'email_response_time') {
            $result = $this->processAverage('email_second_response_time', $internetShop, $noFilter);
            $response['top_second_response_time'] = $result['top'];
            $response['bottom_second_response_time'] = $result['btm'];
        }

        return $response;
    }

    // Copy Pasted from stackoverflow (https://stackoverflow.com/questions/23308584/get-total-time-and-average-time-in-php)
    private function calculateTime($responseTimes)
    {
        $i = 0;
        foreach ($responseTimes as $time) {
            sscanf($time, '%d:%d:%d', $hour, $min, $sec);
            $i += $hour * 3600 + $min * 60 + $sec;
        }

        return $i;
    }

    private function calculateAvg($responseTimes)
    {
        if (count($responseTimes) == 0) {
            return '00:00:00';
        }
        $i = $this->calculateTime($responseTimes);
        $i = round($i / count($responseTimes));
        if ($h = floor($i / 3600)) {
            $i %= 3600;
        }
        if ($m = floor($i / 60)) {
            $i %= 60;
        }

        return sprintf('%02d:%02d:%02d', $h, $m, $i);
    }

    private function processAverage($column, $internetShop, $noFilter = true)
    {
        $secondResponseTimeResults = $internetShop->pluck($column);
        $tenPercent = ! $noFilter ? $this->tenPercent($secondResponseTimeResults->count()) : $secondResponseTimeResults->count();
//        $secondResponseTimeResults = $this->processResponseTime($secondResponseTimeResults);
        $secondResponseTimeResults = $secondResponseTimeResults->filter();
        $responseTimeResultsTop = $secondResponseTimeResults->sort();
        $responseTimeResultsBtm = $secondResponseTimeResults->sortDesc();
        $responseTimeResultsTop = $responseTimeResultsTop->take($tenPercent);
        $responseTimeResultsBtm = $responseTimeResultsBtm->take($tenPercent);
        $avgTop = $this->calculateAvg($responseTimeResultsTop);
        $avgBtm = $this->calculateAvg($responseTimeResultsBtm);

        return [
            'top' => $avgTop,
            'btm' => $avgBtm,
        ];
    }

    /**
     * Parses the $time to a Carbon instance.
     * @param $time
     * @return Carbon
     */
    private function parseTimeToDate($time)
    {
        $date = now()->startOfDay();
        $timeArray = explode(':', $time);

        return $date
            ->addHours($timeArray[0])
            ->addMinutes($timeArray[1])
            ->addSeconds($timeArray[2]);
    }

    private function processResponseTime(Collection $value): Collection
    {
        $responseTimes = $value->filter();
        $responseTimes->transform(function ($item) {
            return $this->parseTimeToDate($item);
        });

        return $responseTimes;
    }

    private function getAverage(int $time, int $count)
    {
        if (empty($time)) {
            return $time;
        }

        return $time / $count;
    }
}
