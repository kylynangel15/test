<?php

namespace App\Services;

use App\Models\User;
use Illuminate\Http\Request;

class SearchUsersService
{
    public function store(Request $request)
    {
        $user = auth()->user();

        // only allow authenticated users and super-admins
        if (
            empty($user) ||
            ! $user->roles->contains('name', 'super-administrator')
        ) {
            abort(403);
        }

        return User::search($request->search)
            ->get()
            ->load('dealer');
    }
}
