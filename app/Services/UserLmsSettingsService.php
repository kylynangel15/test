<?php

namespace App\Services;

use App\Helpers\WithFileUpload;
use App\Models\User;
use App\Models\UserOption;

class UserLmsSettingsService
{
    public function index()
    {
        $currentUserId = auth()->id();
        $lmsOptions = UserOption::where('name', 'LIKE', '%lms_settings%')->where('user_id', $currentUserId)->get();

        return $lmsOptions;
    }

    public function update($request)
    {
        $currentUserId = auth()->id();
        foreach ($request->all() as $key => $value) {
            $realValue = $value['value'];
            $type = $value['type'];
            UserOption::updateOrCreate(
                ['user_id' => $currentUserId, 'name' => 'lms_settings_'.$key],
                [
                    'user_id' => $currentUserId,
                    'name' => 'lms_settings_'.$key,
                    'value' => is_array($realValue) ? json_encode($realValue) : $realValue,
                    'type' => $type,
                ]
            );
        }

        return response()->json([
            'status' => 'Success',
        ], 200);
    }
}
