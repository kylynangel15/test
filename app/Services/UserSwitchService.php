<?php

namespace App\Services;

use App\Models\User;
use Illuminate\Http\Request;

class UserSwitchService
{
    public function store(Request $request)
    {
        $user = auth()->user();

        // only allow authenticated users and super-admins
        if (
            empty($user) ||
            ! $user->roles->contains('name', 'super-administrator')
        ) {
            abort(403);
        }

        $user = User::with([
            'roles',
            'dealer',
        ])
            ->where('id', $request->id)
            ->first();

        $user->aws_url = env('AWS_URL');

        return [
            'token' => auth()->login($user),
            'user' => $user,
        ];
    }
}
