<?php

namespace App\Services;

use App\Helpers\WithFileUpload;
use App\Models\Dealer;
use App\Models\DealerOption;
use App\Models\Role;
use App\Models\SpecificDealer;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Stevebauman\Purify\Facades\Purify;

class DealerService
{
    use WithFileUpload;

    private $dealerIndexName = 'dealer';
    private $optionsIndexName = 'options';
    private $dealers;
    private $dealer;

    private function handleOptions($options)
    {
        unset($options['primary_logo']);
        unset($options['background_image']);
        unset($options['secondary_logo']);

        foreach ($options as $key => $feature) {
            if (empty($feature)) {
                continue;
            }

            $name = $key;
            $lookup = DealerOption::where('dealer_id', $this->dealer->id)
                ->where('name', $name)
                ->first();
            $type = is_bool($feature) ? 'boolean' : 'string';

            $setting = empty($lookup) ? new DealerOption : $lookup;

            if (empty($lookup)) {
                $setting->name = $name;
            }

            $setting->dealer_id = $this->dealer->id;

            if ($setting->name === 'lms_description') {
                $setting->value = Purify::clean($feature);
            } else {
                $setting->value = $feature;
            }

            $setting->type = $type;
            $setting->save();
        }
    }

    public function index()
    {
        $dealer = new Dealer();
        $currentUser = auth()->user();
        $dealers = $dealer->with(['specificDealers.competitors', 'options'])->get();
        if ($currentUser->roles->pluck('name')->contains(Role::SPECIFIC_DEALER_MANAGER)) {
            $specificDealerId = $currentUser->specific_dealer_id;
            //			$dealers = SpecificDealer::with(['dealers'])->where('id', $currentUser->specific_dealer_id)->limit(1)->get();
            $dealers = $dealer::with([
                'specificDealers' => function ($query) use ($specificDealerId) {
                    $query->where('id', $specificDealerId)->get();
                },
                'specificDealers.competitors',
                'options',
            ])->where('id', $currentUser->dealer_id)->get();
        }

        if ($currentUser->roles->pluck('name')->contains(Role::ACCOUNT_MANAGER)) {
            $dealers = $dealer::with(['specificDealers.competitors', 'options'])->where('id', $currentUser->dealer_id)->get();
        }

        return response()->json($dealers);
    }

    public function store($request)
    {
        try {
            $data = $request->{$this->dealerIndexName};
            $data['lms_service'] = (isset($data['lms_service']) && ! is_null($data['lms_service']) && 'allowed' == $data['lms_service']) ? true : false;
            $data['secretshop_service'] = (isset($data['secretshop_service']) && ! is_null($data['secretshop_service']) && 'allowed' == $data['secretshop_service']) ? true : false;
            $this->dealer = Dealer::create($data);
        } catch (\Exception $e) {
            throw $e;
        }

        $this->handleOptions($request->{$this->optionsIndexName});

        $this->handleFileUploads($request->allFiles());

        return response($this->dealer->load('options'));
    }

    private function handleFileUploads($files)
    {
        $dealerId = $this->dealer->id;

        $path = "dealers/{$dealerId}/options/";

        if (isset($files['primaryLogo'])) {
            $this->checkIfRecordIsExistingAndSave(
                'logo_image',
                $path,
                $files['primaryLogo'],
                $dealerId
            );
        }

        if (isset($files['secondaryLogo'])) {
            $this->checkIfRecordIsExistingAndSave(
                'secondary_logo',
                $path,
                $files['secondaryLogo'],
                $dealerId
            );
        }

        if (isset($files['homeBanner'])) {
            $this->checkIfRecordIsExistingAndSave(
                'background_image',
                $path,
                $files['homeBanner'],
                $dealerId
            );
        }
    }

    private function saveToS3($file, $path)
    {
        return $this->saveImageAs($file, $path, 'png', 's3');
    }

    private function checkIfRecordIsExistingAndSave($option, $path, $file, $dealerId)
    {
        $fileName = $this->saveToS3($file, $path);

        if (DealerOption::where('dealer_id', $dealerId)->where('name', $option)->exists()) {
            $this->unlinkOldFile($option, $path, $dealerId);

            return DealerOption::where('dealer_id', $dealerId)
                ->where('name', $option)
                ->update([
                    'value' => Storage::disk('s3')
                    ->url($path.$fileName),
                ]);
        }

        return DealerOption::create([
            'dealer_id' => $dealerId,
            'name' => $option,
            'value'=> Storage::disk('s3')
               ->url($path.$fileName),
            'type' => 'string',
        ]);
    }

    private function unlinkOldFile($option, $path, $dealerId)
    {
        $existingRecord = DealerOption::where('name', $option)
            ->where('id', $dealerId)
            ->first();

        if (is_null($existingRecord) || empty($existingRecord)) {
            return;
        }

        $fileName = Str::afterLast($existingRecord->value, '/');

        return Storage::disk('s3')->delete($path.$fileName);
    }

    public function update($request, $dealer)
    {
        try {
            $data = $request->{$this->dealerIndexName};
            $data['lms_service'] = (isset($data['lms_service']) && ! is_null($data['lms_service']) && 'allowed' == $data['lms_service']) ? true : false;
            $data['secretshop_service'] = (isset($data['secretshop_service']) && ! is_null($data['secretshop_service']) && 'allowed' == $data['secretshop_service']) ? true : false;
            $dealer->update($data);
            $this->dealer = $dealer;
        } catch (\Exception $e) {
            throw $e;
        }

        $this->handleOptions($request->{$this->optionsIndexName});

        // put it here to avoid getting old values for options
        $this->handleFileUploads($request->allFiles());

        return response($this->dealer->load('options'));
    }

    public function destroy($dealer)
    {
        try {
            $dealer->is_active = ! $dealer->is_active;
            $dealer->save();
        } catch (\Exception $e) {
            throw $e;
        }

        return response()->json([
            'message' => 'Dealer successfully deleted.',
        ]);
    }
}
