<?php

namespace App\Services\InternetShop;

use App\Helpers\NationalAverageMtdHelper;
use App\Mail\PreviewInternetShopReport;
use App\Models\DealerOption;
use App\Models\InternetShop;
use App\Models\SpecificDealer;
use Carbon\Carbon;
use Illuminate\Support\Collection;

class PreviewInternetShopReportService
{
    public $internetShop;
    private $nationalavg;

    public function __construct(InternetShop $internetShop)
    {
        $this->internetShop = $internetShop;
        $this->nationalavg = new NationalAverageMtdHelper();
    }

    public function show($id)
    {
        $internetShop = $this->internetShop->with('truecar_fields')->findOrFail($id);

        $image = DealerOption::query()
            ->where('name', 'logo_image')
            ->where('dealer_id', $internetShop->dealer_id)
            ->first();

        $logoBgColor = DealerOption::query()
            ->where('name', 'logo_bg_color')
            ->where('dealer_id', $internetShop->dealer_id)
            ->first();

        if (is_null($image)) {
            $image = 'https://webinarinc-app.s3-us-west-1.amazonaws.com/images/image-a715ffa0-d943-11e9-b53a-43d0016f9823.png';
        } else {
            $image = $image->value;
        }

        if (is_null($logoBgColor)) {
            $logoBgColor = '#4C586A';
        } else {
            $logoBgColor = $logoBgColor->value;
        }

        $internetshops = InternetShop::where('dealer_id', 48)
            ->whereBetween('start_at', [$internetShop->start_at->firstOfMonth(), $internetShop->start_at->endOfMonth()])
            ->get();
        $nationalAverage = [
            'email' =>  $this->nationalavg->getData($internetshops, 'email'),
            'sms' =>  $this->nationalavg->getData($internetshops, 'sms'),
            'call' =>  $this->nationalavg->getData($internetshops, 'call'),
        ];

        $shop = $internetShop;
        $dealerOptions = $shop->dealer->options->map(function ($query) {
            return [
                $query->name => $this->processOptionValue($query->name, $query->value),
            ];
        })->collapse()->toArray();

        return (new PreviewInternetShopReport($shop, $dealerOptions, $image, $logoBgColor, $nationalAverage))->render();
    }

    private function processOptionValue($name, $value)
    {
        // if option is not a response time, then just return the value immediately
        if (strpos($name, 'response_time') === false) {
            return $value;
        }

        $time = $this->parseTimeToDate($value);

        if ($time->hour === 0) {
            return "{$time->minute} minutes";
        }

        $dateNow = now()->startOfDay();
        $hours = $dateNow->diffInHours($time);

        return "{$hours} hours".' and '."{$time->minute} minutes";
    }

    private function parseTimeToDate($time)
    {
        $date = now()->startOfDay();
        $timeArray = explode(':', $time);

        return $date
            ->addHours($timeArray[0])
            ->minutes($timeArray[1]);
    }
}
