<?php

namespace App\Services\LMS\Reminder;

use App\Models\User;

class RefreshService
{
    public static function handle()
    {
        cache()->forget('users-daily-notifications');
        cache()->remember('users-daily-notifications', now()->addDay(), function () {
            return User::with('goals', 'options')->whereHas('roles', function ($query) {
                return $query->where('name', 'salesperson')->orWhere('name', 'account-manager')->orWhere('name', 'specific-dealer-manager');
            })->get();
        });
    }
}
