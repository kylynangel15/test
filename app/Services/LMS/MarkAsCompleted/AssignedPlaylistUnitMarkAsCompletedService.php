<?php

namespace App\Services\LMS\MarkAsCompleted;

use App\Models\AssignedPlaylist;
use App\Models\AssignedPlaylistUnit;
use App\Models\Playlist;
use App\Models\Unit;
use App\Services\LMS\Mailers\CertificateOfCompletionService;
use Carbon\Carbon;
use Illuminate\Support\Facades\Mail;

class AssignedPlaylistUnitMarkAsCompletedService
{
    private $playlistId;
    private $user;
    private $unitId;

    public function markAsCompleted($request)
    {
        $this->playlistId = $request['playlistId'];
        $this->user = auth()->user();
        $this->unitId = $request['unitId'];

        $playlist = AssignedPlaylist::query()
            ->where('playlist_id', $this->playlistId)
            ->where('assignee_id', $this->user->id)
            ->first();

        $assignedPlaylistUnit = AssignedPlaylistUnit::where('assigned_playlist_id', $this->playlistId)
            ->where('unit_id', $this->unitId)
            ->first();

        if (! $playlist && ! $assignedPlaylistUnit) {
            return false;
        }

        if (! $assignedPlaylistUnit && $playlist) {
            $assignedPlaylistUnit = new AssignedPlaylistUnit;
            $assignedPlaylistUnit->unit_id = $this->unitId;
            $assignedPlaylistUnit->assigned_playlist_id = $playlist->id;
        }

        $assignedPlaylistUnit->date_completed = now();
        $assignedPlaylistUnit->save();

        $parentPlaylist = $this->findPlaylist($this->playlistId);

        $parentPlaylist->progress = $this->getProgress($parentPlaylist);

        if ($parentPlaylist->progress >= 100) {
            $mailer = new CertificateOfCompletionService();
            $mailer->send($this->user, $parentPlaylist->name, 'Playlist');
        }

        return $assignedPlaylistUnit;
    }

    public function isCompleted($request)
    {
        return AssignedPlaylist::where('assignee_id', $this->user->id)
            ->whereHas('assignedPlaylistUnit', function ($query) use ($request) {
                $query->where('id', $request['unitId'])->whereNotNull('date_completed');
            })->count();
    }

    private function findPlaylist($id)
    {
        return Playlist::with([
            'units.module',
            'user',
            'assignedPlaylist.user',
            'assignedPlaylist.assignedPlaylistUnit',
            'assignedPlaylist' => function ($query) {
                return $query->where('assignee_id', $this->user->id);
            },
        ])
            ->where('id', $id)
            ->whereIn('user_id', $this->user->dealer->users->pluck('id')->toArray())
            ->first();
    }

    private function getProgress($playlist)
    {
        if ($playlist->assignedPlaylist->isEmpty()) {
            return 0;
        }

        $totalUnits = $playlist->units->count();
        $completedUnits = $this->getCompletedUnits($playlist);
        $totalCompletedUnits = $completedUnits ?: 0;

        if ($totalCompletedUnits < 1) {
            return 0;
        }

        return ($totalCompletedUnits / $totalUnits) * 100;
    }

    private function getCompletedUnits($playlist)
    {
        $completedUnits = $playlist->assignedPlaylist
            ->first()
            ->assignedPlaylistUnit;

        if ($completedUnits->isEmpty()) {
            return $completedUnits;
        }

        return Unit::whereIn('id', $completedUnits->pluck('unit_id')->toArray())->count();
    }
}
