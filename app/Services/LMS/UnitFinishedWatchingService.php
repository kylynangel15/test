<?php

namespace App\Services\LMS;

use App\Models\AssignedPlaylist;
use App\Models\AssignedPlaylistUnit;
use App\Models\UserUnit;
use Illuminate\Http\Request;

class UnitFinishedWatchingService
{
    private $user;

    public function __construct()
    {
        $this->user = auth()->user();
    }

    public function store(Request $request)
    {
        $unit = null;

        if ($request->type === 'assigned') {
            $assignedPlaylist = AssignedPlaylist::where('assignee_id', $this->user->id)
                ->whereHas('assignedPlaylistUnit', function ($query) use ($request) {
                    $query->where('id', $request->unit_id);
                })->first();

            if (! $assignedPlaylist) {
                $unit = new AssignedPlaylistUnit;
                $unit->assigned_playlist_id = $request->playlistId;
                $unit->unit_id = $request->unit_id;
                $unit->finished_watching = true;
                $unit->save();
            } else {
                $unit = AssignedPlaylistUnit::where('assigned_playlist_id', $request->playlistId)
                    ->where('unit_id', $request->unit_id)
                    ->first();
                $unit->finished_watching = true;
                $unit->save();
            }

            return;
        }

        $unit = UserUnit::where('user_id', $this->user->id)
            ->where('unit_id', $request->unit_id)
            ->first();

        if (! $unit) {
            $unit = new UserUnit;
            $unit->user_id = $this->user->id;
            $unit->unit_id = $request->unit_id;
        }

        $unit->finished_watching = 1;
        $unit->save();
    }
}
