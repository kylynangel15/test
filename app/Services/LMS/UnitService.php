<?php

namespace App\Services\LMS;

use App\Helpers\WithFileUpload;
use App\Models\Tag;
use App\Models\Unit;
use HTMLPurifier;
use HTMLPurifier_Config;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use PHPHtmlParser\Dom;
use Stevebauman\Purify\Facades\Purify;

class UnitService
{
    use WithFileUpload;

    public function index($request)
    {
        $perPage = isset($request->per_page) ? $request->per_page : 5;
        $status = $request->type;
        $search = $request->search;
        $sortBy = $request->sortBy;
        $sortDesc = $request->sortDesc;

        return Unit::with(['tags', 'quiz'])
            ->when($search, function ($query, $search) {
                $query->where(
                    'name',
                    'like',
                    "%$search%"
                );
            })
            ->when($status, function ($query, $status) {
                if ($status === 'inactive') {
                    return $query->onlyTrashed();
                } elseif ($status === 'active') {
                    return $query->withoutTrashed();
                } elseif ($status === 'all') {
                    return $query->withTrashed();
                } else {
                    abort(500, 'Invalid filter type...');
                }
            })
            ->when($sortBy, function ($query, $sortBy) use ($sortDesc) {
                $sortDesc = $sortDesc === 'true' ? 'desc' : 'asc';
                if ($sortBy === 'module.name') {
                    return $query->orderByModule($sortDesc);
                } else {
                    return $query->orderBy($sortBy, $sortDesc);
                }
            })
            ->when(empty($sortBy), function ($query) {
                return $query->orderByDesc('created_at');
            })
            ->paginate($perPage);
    }

    public function store($request)
    {
        $unit = new Unit;
        $unit->name = $request->name;
        $unit->module_id = $request->module_id;
        $unit->description = $request->description;
        $unit->video_duration = $request->video_duration;
        $unit->call_guide_link = $request->call_guide_link;
        $unit->content = $this->processHtmlContent($request->content);

        if ($request->quiz_id) {
            $unit->quiz_id = $request->quiz_id;
        }

        $unit->save();

        $fileName = $this->saveImageAs($request->thumbnail, "units/$unit->id/", 'png', 's3');

        $unit->update([
            'thumbnail' => Storage::disk('s3')->url("units/$unit->id/$fileName"),
        ]);

        $tags = $request->tags;

        foreach ($request->tags as $tag) {
            if (! is_numeric($tag)) {
                $tags[] = Tag::create(['name' => $tag])->id;
            }
        }

        $unit->tags()->sync(array_filter($tags, 'is_numeric'));

        return response()->json([
            $unit->load('module', 'quiz'),
        ]);
    }

    public function update($request)
    {
        $unit = Unit::findOrFail($request->id);
        $unit->name = $request->name;
        $unit->module_id = $request->module_id;
        $unit->description = $request->description;
        $unit->video_duration = $request->video_duration;
        $unit->call_guide_link = $request->call_guide_link;
        $unit->content = $this->processHtmlContent($request->content);

        if ($request->quiz_id) {
            $unit->quiz_id = $request->quiz_id;
        }

        $unit->save();

        if ($request->thumbnail && $request->thumbnail !== 'undefined') {
            $oldFile = Str::after($unit->thumbnail, 'com/');

            if (Storage::disk('s3')->exists($oldFile)) {
                Storage::disk('s3')->delete($oldFile);
            }

            $fileName = $this->saveImageAs($request->thumbnail, "units/$unit->id/", 'png', 's3');

            $unit->update([
                'thumbnail' => Storage::disk('s3')->url("units/$unit->id/$fileName"),
            ]);
        }

        $tags = $request->tags;

        foreach ($request->tags as $i => $tag) {
            if (! is_numeric($tag)) {
                $tags[] = Tag::create(['name' => $tag])->id;
                unset($tags[$i]);
            }
        }

        $unit->tags()->sync($tags);

        return response()->json([
            $unit->load('module', 'quiz'),
        ]);
    }

    public function delete($id)
    {
        if (Unit::find($id)->delete()) {
            return response('success');
        }

        return abort(404);
    }

    public function restore($id)
    {
        $unit = Unit::onlyTrashed()->get()->find($id);
        $unit->restore();

        return response()->json(
            $unit->load('module')
        );
    }

    private function processEmbedVideo($content)
    {
        $dom = new Dom;
        $dom->loadStr($content);

        $iframeVideo = $dom->find('iframe', 0);

        if (is_null($iframeVideo)) {
            return $content;
        }

        $iframeVideo->removeAttribute('width');
        $iframeVideo->removeAttribute('height');

        if (! $iframeVideo->hasAttribute('allowfullscreen')) {
            $iframeVideo->setAttribute('allowFullScreen', null);
            $iframeVideo->setAttribute('mozallowfullscreen', null);
            $iframeVideo->setAttribute('webkitAllowFullScreen', null);
        }

        return $dom->outerHtml;
    }

    private function processHtmlContent($content)
    {
        $config = HTMLPurifier_Config::createDefault();
        $config->set('HTML.SafeIframe', true);
        $config->set('URI.SafeIframeRegexp', '%^(https?:)?//(www\.youtube(?:-nocookie)?\.com/embed/|player\.vimeo\.com/video/)%'); //allow YouTube and Vimeo
        $config->set('HTML.Allowed', config('purify.settings.HTML.Allowed'));
        $config->set('CSS.AllowedProperties', config('purify.settings.CSS.AllowedProperties'));

        $def = $config->getHTMLDefinition(true);
        $def->addAttribute('iframe', 'allowfullscreen', 'Bool');

        $purifier = new HTMLPurifier($config);
        $purifiedHtml = $purifier->purify($content);

        return $this->processEmbedVideo($purifiedHtml);
    }
}
