<?php

namespace App\Services\LMS;

use App\Http\Resources\CourseCollection;
use App\Models\Course;
use Facades\App\Services\LMS\CourseDealerService;
use Illuminate\Support\Collection;

class CourseService
{
    public function index($request)
    {
        $perPage = isset($request->per_page) ? $request->per_page : 5;
        $status = $request->type;
        $search = $request->search;
        $sortBy = $request->sortBy;
        $sortDesc = $request->sortDesc;

        return Course::query()
            ->when($search, function ($query, $search) {
                $query->where(
                    'courses.name',
                    'LIKE',
                    "%$search%"
                );
            })
            ->when($sortBy, function ($query, $sortBy) use ($sortDesc) {
                $sortDesc = $sortDesc === 'true' ? 'desc' : 'asc';
                if ($sortBy === 'dealers') {
                    return $query->orderByDealerName($sortDesc);
                } else {
                    return $query->orderBy($sortBy, $sortDesc);
                }
            })
            ->when($status, function ($query, $status) {
                if ($status === 'inactive') {
                    return $query->onlyTrashed();
                } elseif ($status === 'active') {
                    return $query->withoutTrashed();
                } elseif ($status === 'all') {
                    return $query->withTrashed();
                } else {
                    abort(500, 'Invalid filter type...');
                }
            })
            ->paginate($perPage);

//        if ($status == 'inactive') {
//            $course->onlyTrashed();
//        } elseif ($status == 'all') {
//            $course->withTrashed();
//        }
//
//        return $course->paginate($perPage);
    }

    public function store($request)
    {
        $course = new Course;
        $course->name = $request->name;
        $course->save();

        $course->dealers()->sync($request->assigned_dealers);

        return response()->json([
            $course,
        ]);
    }

    public function update($request)
    {
        $course = Course::findOrFail($request->id);
        $course->name = $request->name;
        $course->save();

        $course->dealers()->sync($request->assigned_dealers);

        return response()->json([
            $course,
        ]);
    }

    public function delete($id)
    {
        if (Course::find($id)->delete()) {
            return response('success');
        }

        return abort(404);
    }

    public function restore($id)
    {
        $course = Course::onlyTrashed()->get()->find($id);
        $course->restore();

        return response()->json(
            $course
        );
    }
}
