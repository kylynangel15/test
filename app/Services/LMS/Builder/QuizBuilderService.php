<?php

namespace App\Services\LMS\Builder;

use App\Models\Quiz;
use App\Models\QuizQuestion;
use App\Models\QuizQuestionAnswer;
use App\Models\Unit;
use App\Models\UserAnsweredQuiz;

class QuizBuilderService
{
    public function index($request)
    {
        $status = $request->type;
        $sortBy = $request->sortBy;
        $sortDesc = $request->sortDesc;

        if (empty($status)) {
            abort(500, 'Invalid filter type...');
        }

        return Quiz::query()
            ->with([
                'questions.answers',
            ])
            ->when($request->search, function ($query, $search) {
                $query->where('title', 'LIKE', '%'.$search.'%');
            })
            ->when($status, function ($query, $status) {
                if ($status === 'inactive') {
                    return $query->onlyTrashed();
                } elseif ($status === 'active') {
                    return $query->withoutTrashed();
                } elseif ($status === 'all') {
                    return $query->withTrashed();
                }
            })
            ->when($sortBy, function ($query, $sortBy) use ($sortDesc) {
                $sortDesc = $sortDesc === 'true' ? 'desc' : 'asc';
                if ($sortBy === 'answers') {
                    return $query->withCount('questions')->orderBy('questions_count', $sortDesc);
                } else {
                    return $query->orderBy($sortBy, $sortDesc);
                }
            })
            ->when(! isset($sortBy), function ($query) {
                $query->orderByDesc('created_at');
            })
            ->paginate($request->get('per_page') ?? 5);
    }

    public function show($id)
    {
        return Quiz::with([
            'questions.answers',
        ])->withTrashed()->find($id);
    }

    public function store($request)
    {
        $quiz = Quiz::create([
            'title' => $request->title,
        ]);

        collect($request->questions)->each(function ($question) use ($quiz) {
            $questionRecord = QuizQuestion::create([
                'quiz_id' => $quiz->id,
                'value' => $question['value'],
            ]);

            $this->saveToQuizQuestionAnswer($question['answers'], $questionRecord->id);
        });

        return response()->json([
            'success' => true,
        ]);
    }

    public function saveToQuizQuestionAnswer($answers, $questionId)
    {
        collect($answers)->each(function ($answer) use ($questionId) {
            QuizQuestionAnswer::create([
                'quiz_question_id' => $questionId,
                'value' => $answer['value'],
                'is_correct' => isset($answer['is_correct']) ? true : false,
            ]);
        });
    }

    public function update($id, $request)
    {
        $quiz = Quiz::find($id);
        $quiz->title = $request['title'];
        $quiz->save();

        $existingQuestionsIdFromRequest = (collect($request['questions']))->map(function ($question) {
            if (isset($question['id'])) {
                return $question['id'];
            }

            return null;
        })->toArray();

        $existingQuestionsIdFromDatabase = QuizQuestion::where('quiz_id', $id)->pluck('id')->toArray();

        $removedQuestionsId = array_diff($existingQuestionsIdFromDatabase, $existingQuestionsIdFromRequest);

        QuizQuestion::destroy($removedQuestionsId);

        collect($request['questions'])->each(function ($question) use ($id) {
            $questionRecord = QuizQuestion::updateOrCreate(['id' => isset($question['id']) ? $question['id'] : ''],
                [
                    'quiz_id' => $id,
                    'value' => $question['value'],
                ]
            );

            if (isset($question['id'])) {
                $oldAnswers = QuizQuestionAnswer::query()
                    ->where('quiz_question_id', $question['id'])
                    ->pluck('id')
                    ->toArray();
                $currentAnswers = collect($question['answers'])->pluck('id')->toArray();
                $removedAnswers = array_diff($oldAnswers, $currentAnswers);
                QuizQuestionAnswer::destroy($removedAnswers);
            }

            collect($question['answers'])->filter()->each(function ($answer) use ($questionRecord) {
                QuizQuestionAnswer::updateOrCreate([
                    'id' => isset($answer['id']) ? $answer['id'] : '',
                    'quiz_question_id' => $questionRecord->id,
                ], [
                    'quiz_question_id' => $questionRecord->id,
                    'value' => $answer['value'],
                    'is_correct' => $this->isAnswerCorrect($answer),
                ]);
            });
        });

        return response()->json([
            'success' => true,
            'data' => Quiz::with('questions.answers')->find($id),
        ]);
    }

    public function isAnswerCorrect($answer)
    {
        if (isset($answer['isCorrect']) && $answer['isCorrect'] === true
            || isset($answer['is_correct']) && $answer['is_correct'] == true) {
            return true;
        }

        return false;
    }

    public function destroy($id)
    {
        $removeQuiz = Quiz::find($id)->delete();

        if ($removeQuiz) {
            $this->deleteUserAnswers($id);

            return response()->json([
                'success' => true,
            ]);
        }

        abort(500, 'Can\'t archive this quiz');
    }

    private function deleteUserAnswers($quizId)
    {
        $units = Unit::where('quiz_id', $quizId)
            ->get();

        $units->each(function ($unit) use ($quizId) {
            $unit->quiz_id = null;
            $unit->save();

            UserAnsweredQuiz::query()
                ->where('unit_id', $unit->id)
                ->where('quiz_id', $quizId)
                ->delete();
        });
    }

    public function restore($id)
    {
        Quiz::withTrashed()
            ->find($id)
            ->restore();

        $userAnsweredQuizzes = UserAnsweredQuiz::withTrashed()
            ->where('quiz_id', $id)
            ->get();

        $userAnsweredQuizzes->each(function ($quiz) {
            $quiz->restore();
        });

        return response()->json([
            'success' => true,
        ]);
    }
}
