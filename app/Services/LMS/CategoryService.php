<?php

namespace App\Services\LMS;

use App\Helpers\WithFileUpload;
use App\Models\Category;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class CategoryService
{
    use WithFileUpload;

    public function index($request)
    {
        $perPage = isset($request->per_page) ? $request->per_page : 5;
        $status = $request->type;
        $search = $request->search;
        $sortBy = $request->sortBy;
        $sortDesc = $request->sortDesc;

        return Category::query()
            ->when($search, function ($query, $search) {
                $query->where(
                    'categories.name',
                    'like',
                    "%$search%"
                );
            })
            ->when($status, function ($query, $status) {
                if ($status === 'inactive') {
                    return $query->onlyTrashed();
                } elseif ($status === 'active') {
                    return $query->withoutTrashed();
                } elseif ($status === 'all') {
                    return $query->withTrashed();
                } else {
                    abort(500, 'Invalid filter type...');
                }
            })
            ->when($sortBy, function ($query, $sortBy) use ($sortDesc) {
                $sortDesc = $sortDesc === 'true' ? 'desc' : 'asc';
                if ($sortBy === 'course.name') {
                    return $query->orderByCourse($sortDesc);
                } else {
                    return $query->orderBy($sortBy, $sortDesc);
                }
            })
            ->when(empty($sortBy), function ($query) {
                return $query->orderByDesc('created_at');
            })
            ->paginate($perPage);
    }

    public function store($request)
    {
        $category = new Category;
        $category->name = $request->name;
        $category->course_id = $request->course;
        $category->save();

        $fileName = $this->saveImageAs($request->thumbnail, "categories/$category->id/", 'jpg', 's3');

        $category->update([
            'thumbnail' => Storage::disk('s3')->url("categories/$category->id/$fileName"),
        ]);

        return response()->json([
            $category,
        ]);
    }

    public function update($request)
    {
        $category = Category::find($request['id']);
        $category->name = $request->name;
        $category->course_id = $request->course;
        $category->save();

        if ($request->thumbnail && $request->thumbnail !== 'undefined') {
            $oldFile = Str::after($category->thumbnail, 'com/');

            if (Storage::disk('s3')->exists($oldFile)) {
                Storage::disk('s3')->delete($oldFile);
            }

            $fileName = $this->saveImageAs($request->thumbnail, "categories/$category->id/", 'jpg', 's3');

            $category->update([
                'thumbnail' => Storage::disk('s3')->url("categories/$category->id/$fileName"),
            ]);
        }

        return response()->json([
            $category,
        ]);
    }

    public function show($id)
    {
        return Category::with(['courses.course', 'excludedUsers.user'])->find($id);
    }

    public function destroy($id)
    {
        if (Category::destroy($id)) {
            return response('success');
        }
        abort(404);
    }

    public function restore($id)
    {
        $category = Category::onlyTrashed()->get()->find($id);
        $category->restore();

        return response()->json(
            $category
        );
    }

    public function fetchCategoriesForSelection()
    {
        $categories = Category::without('course')->select('id', 'name')->get();

        return response()->json($categories);
    }
}
