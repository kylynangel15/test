<?php

namespace App\Services;

use App\Helpers\UserAcessControlChecker;
use App\Models\Role;
use App\Models\User;

class GetRandomUserService
{
    use UserAcessControlChecker;

    public function getUsers($numberOfUsers, $search = null)
    {
        return User::query()
            ->when($search, function ($query, $search) {
                $query->orWhere('name', 'LIKE', "%$search%");
            })
            ->where(function ($query) {
                if ($this->isSpecificDealerManager()) {
                    return $query->where('specific_dealer_id', auth()->user()->specific_dealer_id)
                        ->where('dealer_id', auth()->user()->dealer_id);
                }

                if ($this->isAccountManager()) {
                    return $query->where('dealer_id', auth()->user()->dealer_id);
                }

                if ($this->isSalesperson() && empty(auth()->user()->specific_dealer_id)) {
                    return $query->where('dealer_id', auth()->user()->dealer_id);
                }

                if ($this->isSalesperson() && ! empty(auth()->user()->specific_dealer_id)) {
                    return $query->where('specific_dealer_id', auth()->user()->specific_dealer_id)
                        ->where('dealer_id', auth()->user()->dealer_id);
                }
            })
            ->orderByDesc('created_at')
            ->paginate($numberOfUsers);
    }
}
