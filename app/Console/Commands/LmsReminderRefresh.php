<?php

namespace App\Console\Commands;

use App\Services\LMS\Reminder\RefreshService;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class LmsReminderRefresh extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'lms-reminder:refresh';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Clears the cache database result and will get the current records in database for users and goals.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            $this->info('Refreshing...');
            RefreshService::handle();
            $this->info('Successfully refreshed!');
        } catch (\Exception $e) {
            Log::info('Unable to refresh the lms reminder data. Error: '.$e->getMessage());
        }
    }
}
