<?php

namespace App\Helpers;

use App\Models\InternetShop;
use Illuminate\Support\Collection;

class NationalAverageMtdHelper
{
    public function getData($internetShop, $column)
    {
        $columnAttempts = $column.'_attempts';
        $columnResponseTime = $column.'_response_time';

        if ($column == 'email') {
            $columnResponseTime = 'email_second_response_time';
        }

        $tenPercentC = $this->tenPercent($internetShop->count());

        $attemptResults = $internetShop->pluck($columnAttempts);

        $attemptResultsTop = $attemptResults->sort()->reverse();
        $attemptResultsBtm = $attemptResults->sort();
        $attemptResultsTop = $attemptResultsTop->reject(function ($attempts) {
            return $attempts === 0;
        })->take($tenPercentC)->sum();

        $attemptResultsBtm = $attemptResultsBtm->reject(function ($attempts) {
            return $attempts === 0;
        })->take($tenPercentC)->sum();

        $responseTimeResults = $internetShop->pluck($columnResponseTime);

        $tenPercent = $this->tenPercent($responseTimeResults->count());
        $responseTimeResults = $responseTimeResults->filter();

        $responseTimeResultsTop = $responseTimeResults->sort();
        $responseTimeResultsBtm = $responseTimeResults->sortDesc();

        $responseTimeResultsTop = $responseTimeResultsTop->take($tenPercent);
        $responseTimeResultsBtm = $responseTimeResultsBtm->take($tenPercent);

        $tenPercent = $tenPercentC;
        $attemptResultsTop = $attemptResultsTop !== 0 ? intval(ceil($attemptResultsTop / $tenPercent)) : 0;
        $attemptResultsBtm = $attemptResultsBtm !== 0 ? intval(ceil($attemptResultsBtm / $tenPercent)) : 0;

        $avgTop = $this->calculateAvg($responseTimeResultsTop);
        $avgBtm = $this->calculateAvg($responseTimeResultsBtm);

        $response = [
            'top_attempts'  =>  $attemptResultsTop,
            'bottom_attempts'   =>  $attemptResultsBtm,
            'top_response_time' =>  $avgTop,
            'top_second_response_time' => '00:00:00',
            'bottom_second_response_time' => '00:00:00',
            'bottom_response_time'  =>  $avgBtm,
            'tpc'   =>  $tenPercentC,
            'tp'    =>  $tenPercent,
        ];

        if ($columnResponseTime == 'email_response_time') {
            $result = $this->processAverage('email_second_response_time', $internetShop);
            $response['top_second_response_time'] = $result['top'];
            $response['bottom_second_response_time'] = $result['btm'];
        }

        return $response;
    }

    private function tenPercent($count): int
    {
        $count = $count * .10;

        return ceil($count);
    }

    private function processAverage($column, $internetShop)
    {
        $secondResponseTimeResults = $internetShop->pluck($column);
        $tenPercent = $this->tenPercent($secondResponseTimeResults->count());
        $secondResponseTimeResults = $secondResponseTimeResults->filter();
        $responseTimeResultsTop = $secondResponseTimeResults->sort();
        $responseTimeResultsBtm = $secondResponseTimeResults->sortDesc();
        $responseTimeResultsTop = $responseTimeResultsTop->take($tenPercent);
        $responseTimeResultsBtm = $responseTimeResultsBtm->take($tenPercent);
        $avgTop = $this->calculateAvg($responseTimeResultsTop);
        $avgBtm = $this->calculateAvg($responseTimeResultsBtm);

        return [
            'top' => $avgTop,
            'btm' => $avgBtm,
        ];
    }

    private function calculateTime($responseTimes)
    {
        $i = 0;
        foreach ($responseTimes as $time) {
            sscanf($time, '%d:%d:%d', $hour, $min, $sec);
            $i += $hour * 3600 + $min * 60 + $sec;
        }

        return $i;
    }

    private function calculateAvg($responseTimes): string
    {
        if (count($responseTimes) == 0) {
            return '00:00:00';
        }
        $i = $this->calculateTime($responseTimes);
        $i = round($i / count($responseTimes));
        if ($h = floor($i / 3600)) {
            $i %= 3600;
        }
        if ($m = floor($i / 60)) {
            $i %= 60;
        }

        return sprintf('%02d:%02d:%02d', $h, $m, $i);
    }
}
