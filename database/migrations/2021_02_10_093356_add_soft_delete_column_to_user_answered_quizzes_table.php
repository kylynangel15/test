<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddSoftDeleteColumnToUserAnsweredQuizzesTable extends Migration
{
    public function up()
    {
        Schema::table('user_answered_quizzes', function (Blueprint $table) {
            $table->softDeletes();
        });
    }

    public function down()
    {
        Schema::table('user_answered_quizzes', function (Blueprint $table) {
            $table->dropSoftDeletes();
        });
    }
}
