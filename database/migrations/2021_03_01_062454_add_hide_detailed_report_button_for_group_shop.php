<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddHideDetailedReportButtonForGroupShop extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('group_shops', function (Blueprint $table) {
            $table->boolean('hide_detailed_report_btn')->default(0)->after('hide_dealer_name');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('group_shops', function (Blueprint $table) {
            $table->dropColumn('hide_detailed_report_btn');
        });
    }
}
