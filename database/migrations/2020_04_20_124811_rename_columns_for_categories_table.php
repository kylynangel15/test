<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RenameColumnsForCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        /*Schema::table('categories', function (Blueprint $table) {
            $table->renameColumn('value', 'name');
            $table->renameColumn('banner_link', 'thumbnail');
        });*/

        // sqlite does not accept multiple rename column and produce error when testing
        // this is one of the solution found in this issue link: https://github.com/laravel/framework/issues/2979#issuecomment-597968878
        collect([
            ['value', 'name'],
            ['banner_link', 'thumbnail'],
        ])->map(function ($old_new) {
            Schema::table('categories', function (Blueprint $table) use ($old_new) {
                $table->renameColumn($old_new[0], $old_new[1]);
            });
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('categories', function (Blueprint $table) {
            $table->renameColumn('name', 'value');
            $table->renameColumn('thumbnail', 'banner_link');
        });
    }
}
