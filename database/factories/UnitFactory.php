<?php

namespace Database\Factories;

use App\Models\Unit;
use Illuminate\Database\Eloquent\Factories\Factory;

class UnitFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Unit::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'thumbnail' => $this->faker->imageUrl(800, 448, 'video', true),
            'description' => $this->faker->paragraph,
            'video_duration' => $this->faker->time(),
            'content' => $this->faker->randomHtml(),
            'name' => $this->faker->sentence,
        ];
    }
}
