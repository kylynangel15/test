<?php

namespace Database\Factories;

use App\Models\Dealer;
use Illuminate\Database\Eloquent\Factories\Factory;

class DealerFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Dealer::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name'                => $this->faker->company,
            'website'             => $this->faker->url,
            'address'             => $this->faker->address,
            'is_automotive'       => $this->faker->boolean(),
            'is_active'           => 1,
            'lms_service'         => 1,
            'secretshop_service'  => 1,
        ];
    }
}
