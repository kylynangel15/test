<?php

namespace Database\Factories;

use App\Models\Dealer;
use App\Models\PhoneNumber;
use App\Models\State;
use App\Models\User;
use App\Models\VoiceMail;
use Illuminate\Database\Eloquent\Factories\Factory;

class PhoneNumberFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = PhoneNumber::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'state_id'      => State::factory(),
            'area_codes'    => $this->faker->areaCode,
            'value'         => $this->faker->tollFreePhoneNumber,
            'formatted_value'         => $this->faker->tollFreePhoneNumber,
            'dealer_id'     => Dealer::factory(),
            'voice_mail_id' => $this->faker->randomElement(
                VoiceMail::pluck('id')->toArray()
            ),
            'is_dealer'     => $this->faker->boolean(60),
            'user_id'       => User::factory(),
        ];
    }
}
