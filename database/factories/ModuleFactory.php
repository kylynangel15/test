<?php

namespace Database\Factories;

use App\Models\Module;
use Illuminate\Database\Eloquent\Factories\Factory;

class ModuleFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Module::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->sentence,
            'thumbnail' => $this->faker->imageUrl(800, 448, 'video', true),
            'description' => $this->faker->paragraph,
            'call_guide_link' => $this->faker->url,
        ];
    }
}
